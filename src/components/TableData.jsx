import React from 'react'
import { Container, Table, Button, Badge } from 'react-bootstrap'

function TableData(props) {

    return (
        <Container>         
            <Button 
                variant="primary"
                className="my-3"
                onClick={()=>
                    props.onReset()
                }>Reset</Button>
            <Badge variant="warning" className="mx-2">count {props.items[3].id}</Badge>

            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Food</th>
                    <th>Amount</th>
                    <th>Price</th>
                    <th>Total</th>
                    </tr>
                </thead>
                    <tbody>
                        {   
                            props.items.map((item,i) =>(
                            <tr key={i}>
                                {
                                    <td>{item.id}</td>
                                }
                                {
                                    <td>{item.title}</td>
                                }
                                {
                                    <td>{item.amount}</td>
                                }
                                {
                                    <td>{item.price}</td>
                                }
                                {
                                    <td>{item.total}</td>
                                }
                            </tr>
                            ))
                        }
                    </tbody>
                </Table>
        </Container>
    )
}

export default TableData
